##Instructions :

Setup Dev Environement

Installer Docker , Docker Compose, pour Windows, installer Docker Desktop


```
docker-compose up
```

```
npm install
npm run hot
```



##Prerequis


PHP
```
sudo apt update
sudo apt install -y lsb-release ca-certificates apt-transport-https software-properties-common gnupg2
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/sury-php.list
wget -qO - https://packages.sury.org/php/apt.gpg | sudo apt-key add -
sudo apt update
sudo apt install -y php8.1
sudo apt install -y php8.1-curl php8.1-gd php8.1-mysql php8.1-xml php8.1-zip
```

NODE.js

```
curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
apt-get install -y nodejs
```

Installer MySQL, pour cela il faut installer WAMP ou docker desktop. 


`cp .env.example .env`  


Remplir le fichier .env avec les informations de la bdd locale
Remplir également avec les identifiants smtp et les infos websockets  

`composer install`  
`npm install`  
`php artisan migrate` pour déployer les tables  
`php artisan key:generate`

Pour run le projet:  
`npm run hot`  
`php artisan serve`
