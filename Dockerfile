FROM php:8.1.6-cli

WORKDIR /opt/webapp

COPY . /opt/webapp

RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions

RUN install-php-extensions gd mysqli pdo pdo_mysql xml zip

RUN composer install