<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Admin
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin query()
 */
	class Admin extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Candidature
 *
 * @property int $id
 * @property string $email
 * @property string|null $childFirstName
 * @property string|null $childLastName
 * @property string|null $birthPlace
 * @property string|null $birthDate
 * @property string|null $parentFirstName
 * @property string|null $parentLastName
 * @property int|null $lastStep
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature query()
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereBirthPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereChildFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereChildLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereLastStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereParentFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereParentLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidature whereUpdatedAt($value)
 */
	class Candidature extends \Eloquent {}
}

