const storeAdmin={
    namespaced: true,
    state:{
        logged:false,
        candidatures:null,
    },
    mutations:{
        setData(state, payload) {
            state[payload[0]] = payload[1];
        },
    },
    getters:{
        store: state => state,
    }
}
export default storeAdmin;



