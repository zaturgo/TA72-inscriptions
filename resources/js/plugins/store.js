import Vue from 'vue'
import Vuex from 'vuex'
import storeAdmin from "./storeAdmin";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        lastStep:null,
        id:null,
        email:null,

        motivation:null,

        c_name:null,
        c_surname:null,
        c_birthday:null,
        c_birthplace:null,
        c_sex:null,

        rl1_name:null,
        rl1_surname:null,
        rl1_birthday:null,
        rl1_birthplace:null,
        rl1_sex:null,
        rl1_address:null,
        rl1_postcode:null,
        rl1_city:null,
        rl1_tel:null,
        rl1_mail:null,

        rl2_name:null,
        rl2_surname:null,
        rl2_birthday:null,
        rl2_birthplace:null,
        rl2_sex:null,
        rl2_address:null,
        rl2_postcode:null,
        rl2_city:null,
        rl2_tel:null,
        rl2_mail:null,

        c_class:null,
        c_lastclass:null,

        cl_adress:null,
        cl_postcode:null,
        cl_city:null,

        i_gardmat:false,
        i_gardapr:false,
        i_rest:false,
        i_trans:false,

        // doc_assur:false,
        // doc_livret:false,

    },
    mutations: {
        setData(state, payload) {
            localStorage.setItem(payload[0], payload[1]);
            state[payload[0]] = payload[1];
        },
        initialiseStore(state) {
            Object.entries(state).forEach(item=>{
                if (state[item[0]]==null)state[item[0]] = localStorage.getItem(item[0]);
            })
        },
        async setCurrentCandidature(state,payload) {
            let c = await axios.get('/candidature/'+payload)
            Object.entries(state).forEach(item=>{
                    if (item[0]!=='storeAdmin') {
                        state[item[0]] = c.data[item[0]];
                    }
            })
        },
        resetStore(state){
            Object.entries(state).forEach(item=>{
                if (item[0]!=='storeAdmin'){
                    state[item[0]] = null;
                    localStorage.removeItem(item[0]);
                }
            })
        }
    },
    actions: {

    },
    modules: {
        storeAdmin
    },
    getters:{
        store: state => state,

    }
})
