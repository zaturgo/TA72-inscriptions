<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('candidature/export', '\App\Http\Controllers\CandidatureController@export');
Route::resource('candidature', \App\Http\Controllers\CandidatureController::class);
Route::resource('candidature.messages', App\Http\Controllers\Candidature\MessageController::class);
Route::resource('candidature.meetings', \App\Http\Controllers\Candidature\MeetingController::class);
Route::post('/admin/login','\App\Http\Controllers\AdminController@login');
Route::resource('admin',\App\Http\Controllers\AdminController::class);
Route::resource('candidature.files', \App\Http\Controllers\Candidature\FileUploadController::class);
Route::post('resetLink','\App\Http\Controllers\CandidatureController@resetLink');
Route::post('candidature/{candidature}/files/{file}/add','\App\Http\Controllers\Candidature\FileUploadController@addFile');
